<?php
/**
 * Plugin Name: WordPress Loves Amazon S3
 * Plugin URI: http://github.com/fredriksundstrom/wp-loves-s3
 * Description: This plugin is the most bad-ass S3 plugin for WordPress. It doesn't just copy and store and serve your images from S3 - it's a complete take-over which lets you edit files etc. in WordPress as normal.
 * Version: 0.0.1
 * Author: Fredrik Sundström
 * Author URI: http://github.com/fredriksundstrom
 * License: MIT
 * Namespace: wpls
 */

require(__DIR__ . '/vendor/autoload.php');

use Aws\Common\Aws;

if (!class_exists('WPLS'))
{
  class WPLS
  {
    var $client;

    public function __construct()
    {
      Dotenv::load(__DIR__);
      Dotenv::required(array('TESTS_S3_KEY', 'TESTS_S3_SECRET', 'TESTS_S3_REGION', 'TESTS_S3_BUCKET'));

      $aws = Aws::factory(array(
        'key'    => getenv('TESTS_S3_KEY'),
        'secret' => getenv('TESTS_S3_SECRET'),
        'region' => getenv('TESTS_S3_REGION')
      ));
      $this->client = $aws->get('s3');
      $this->bucket = getenv('TESTS_S3_BUCKET');
    }

    public function activate()
    {
      $my_bucket_exists = $this->client->headBucket(array('Bucket' => $this->bucket));

      if (!$my_bucket_exists) {
        $result = $this->client->createBucket(array(
          'Bucket'             => $this->bucket,
          'LocationConstraint' => \Aws\Common\Enum\Region::EU_WEST_1
        ));
        $this->client->waitUntilBucketExists(array('Bucket' => $this->bucket));
      }
    }
    
    public function deactivate() {}

    # return apply_filters( 'wp_handle_upload', array( 'file' => $new_file, 'url' => $url, 'type' => $type ), 'upload' );

    /*public static function handle_upload($args, $type)
    {
      $file = $args['file'];
      $url  = $args['url'];
      $type = $args['type'];

      error_log("handle_upload: {var_export($file)} / {var_export($url)} / {var_export($type)} / {var_export($type)}");

      error_log("handle_upload, file_is_displayable_image: {file_is_displayable_image($file)");

      if ( file_is_displayable_image($file) ) {
        error_log("handle_upload, if file_is_displayable_image: TRUE!");

        wp_update_attachment_metadata( $id, wp_generate_attachment_metadata( $id, $file ) );
      }

      return $args;
    }*/

    public function get_attachment_url($url, $post_id)
    {
      return $url;
    }

    public function get_attached_file($file, $attachment_id)
    {
      return $file;
    }

    public function update_attached_file($file, $attachment_id)
    {
      $result = $this->client->putObject(array(
        'Bucket'     => $this->bucket,
        'Key'        => basename($file),
        'SourceFile' => $file,
        'Metadata'   => array(
          'Foo' => 'abc',
          'Baz' => '123'
        )
      ));

      $this->client->waitUntilObjectExists(array(
        'Bucket' => $this->bucket,
        'Key'    => basename($file)
      ));

      $result = $this->client->getObject(array(
        'Bucket' => $this->bucket,
        'Key'    => basename($file)
      ));

      #die("TJA");

      #echo get_class($result['Body']) . "\n";

      if (file_is_displayable_image($file)) {
        #error_log("update_attached_file, if file_is_displayable_image: TRUE!");

        wp_update_attachment_metadata( $attachment_id, wp_generate_attachment_metadata($attachment_id, $file));
      }

      return $file;
    }

    # <?php wp_delete_attachment( $attachmentid, $force_delete ); 

    public function delete_attachment($post_id)
    {
      $attachment = get_post($post_id);

      $this->client->deleteObject(array('Bucket' => $this->bucket, 'Key' => basename($attachment->guid)));
    }

    public function file_exists($post_id)
    {
      $attachment = get_post($post_id);

      $my_object_exists = $this->client->headObject(array('Bucket' => $this->bucket, 'Key' => basename($attachment->guid)));
    
      return is_object($my_object_exists);
    }
  }
}

if (class_exists('WPLS'))
{
  $wpls = new WPLS();

  register_activation_hook(__FILE__, array($wpls, 'activate'));
  register_deactivation_hook(__FILE__, array($wpls, 'deactivate'));

  #add_filter('wp_handle_upload', array('WPLS', 'handle_upload'), 10, 3);
  add_filter('wp_get_attachment_url', array($wpls, 'get_attachment_url'), 10, 2);
  add_filter('get_attached_file', array($wpls, 'get_attached_file'), 10, 2);
  add_filter('update_attached_file', array($wpls, 'update_attached_file'), 10, 2);
  add_action('delete_attachment', array($wpls, 'delete_attachment'));
}
