<?php

$I = new WebGuy($scenario);
$I->wantTo('upload an image and find it in my S3 bucket');
$I->amOnPage('/wp/wp-admin');
$I->fillField('#user_login', 'test');
$I->fillField('#user_pass','test');
$I->click('#wp-submit');
$I->amOnPage('/wp/wp-admin/media-new.php?browser-uploader');
$I->see("You are using the browser’s built-in file uploader");
$I->attachFile('#async-upload', 'nice_image.jpg');
$I->click('html-upload');
$I->seeTheAttachmentIJustCreated();
$I->seeTheAttachmentIJustCreatedInMyBucket();
$I->amOnPage('/wp/wp-admin/upload.php');
$I->selectOption('.tablenav.top .bulkactions select', 'delete');
$I->checkOption('#the-list input[type="checkbox"]');
$I->click('.tablenav.top #doaction');