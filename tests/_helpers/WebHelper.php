<?php
namespace Codeception\Module;

// here you can define custom functions for WebGuy 

class WebHelper extends \Codeception\Module
{
  public function _initialize()
  {
    require('../zend_test/wp/wp-load.php');
  }

  public function seeTheAttachmentIJustCreated()
  {
    $args = array(
      'post_type'      => 'attachment',
      'numberposts'    => 1,
    );

    $attachments = get_posts($args);

    $is = count($attachments);

    $this->assertEquals(1, $is, "Attachment count should be 1, not $is.");
  }

  public function seeTheAttachmentIJustCreatedInMyBucket()
  {
    $args = array(
      'post_type'      => 'attachment',
      'numberposts'    => 1,
    );

    $attachments = get_posts($args);

    $wpls = new \WPLS();

    $this->assertTrue($wpls->file_exists($attachments[0]->ID), "Attachment wasn't found in your S3 bucket.");
  }
}
